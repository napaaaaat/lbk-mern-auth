import { createContext, useEffect, useReducer } from "react";
import jwt_decode from "jwt-decode";

import AuthReducer from "../Reducer/AuthReducer";
import setAuthToken from "../../utils/setAuthToken";
import { Logout } from "../Action/AuthAction";

const INITIAL_AUTH_STATE = {
    user : JSON.parse(localStorage.getItem('user')) || null,
    isAuthenticated : false,
    loading : true,
    error : null
};

export const AuthContext = createContext(INITIAL_AUTH_STATE);

export const AuthProvider = ({ children }) => {
    const [state, AuthDispatch] = useReducer(AuthReducer, INITIAL_AUTH_STATE);

    const checkExpiredToken = () => {
        const token = JSON.parse(localStorage.getItem('user'));
        if (token) {
            const decoded = jwt_decode(token);
            if (decoded.exp < Date.now() / 1000) {
                localStorage.removeItem('user');
                console.log('token expired');
                return AuthDispatch(
                    Logout()
                );
            }
            return 
        }
        return 
    }

    useEffect(() => {
        localStorage.setItem('user', JSON.stringify(state.user));
        checkExpiredToken();

    }, [state.user]);

    if(state.user){
        setAuthToken(state.user);
    };

    return (
        <AuthContext.Provider value={{
            user : state.user,
            isAuthenticated : state.isAuthenticated,
            loading : state.loading,
            error : state.error,
            dispatch: AuthDispatch
        }}>
            {children}
        </AuthContext.Provider>
    );
};
