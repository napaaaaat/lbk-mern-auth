import { LOGIN_START, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "../TypesConstant";

const AuthReducer = (state, action) => {

    const { type, payload } = action;

    switch (type) {
        case LOGIN_START: 
            return {
                user: null,
                isAuthenticated: false,
                loading: true,
                error: null
            };
        case LOGIN_SUCCESS: 
            return {
                user: payload,
                isAuthenticated: true,
                loading: false,
                error: null
            };
        case LOGIN_FAIL: 
            return {
                user: null,
                isAuthenticated: false,
                loading: false,
                error: payload
            };
        case LOGOUT: 
            return {
                user: null,
                isAuthenticated: false,
                loading: false,
                error: null
            };
        default: {
            return state;
        }
    }
};

export default AuthReducer;
