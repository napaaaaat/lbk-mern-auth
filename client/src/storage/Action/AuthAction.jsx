import { LOGIN_START, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from "../TypesConstant";

export const LoginStart = () => ({
    type: LOGIN_START
});

export const LoginSuccess = (user) => ({
    type: LOGIN_SUCCESS,
    payload: user
});

export const LoginFailure = (error) => ({
    type: LOGIN_FAIL,
    payload: error
});

export const Logout = () => ({
    type: LOGOUT
});