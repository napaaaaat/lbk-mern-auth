import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';

import { AuthContext } from '../storage/Context/AuthContext';

function ProtectRoute({ children }) {
  const { user } = useContext(AuthContext);

  return !user ? children : <Navigate to="/" /> 
  // react router v6
}

export default ProtectRoute;
