export const API_PATH = {
    LOGIN: '/user/signin',
    REGISTER: '/user/register',
    PROFILE: '/profile/me',
}