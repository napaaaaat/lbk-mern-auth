import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';

import { AuthContext } from '../storage/Context/AuthContext';

function AuthRoute({ children }) {
  const { user } = useContext(AuthContext);

  return user ? children : <Navigate to="/login" /> 
  // react router v6
}

export default AuthRoute;
