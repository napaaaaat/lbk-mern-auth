import { useContext, useState } from 'react';
import { Navigate } from 'react-router-dom';
import axios from 'axios';

import { Avatar, Button, Grid, Paper, Typography, TextField, Link, CssBaseline, Box } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { AuthContext } from '../storage/Context/AuthContext';
import { AlertContext } from '../storage/Context/AlertContext';
import { LoginStart, LoginSuccess, LoginFailure } from '../storage/Action/AuthAction';
import { AlertShow } from '../storage/Action/AlertAction';
import { API_PATH } from '../utils/APIConstant';

function Login() {

	const [formData, setFormData] = useState({
		email: "",
		password: "",
	})

	const { dispatch, isLoading } = useContext(AuthContext);
	const { alerts, AlertDispatch } = useContext(AlertContext);

	console.log("alerts", alerts);

	const { email, password } = formData;

	const onChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const onSubmit = async (e) => {
		e.preventDefault();

		dispatch(LoginStart());

		try {
			const res = await axios.post(`${API_PATH.LOGIN}`, { email, password });

			dispatch(LoginSuccess(res.data.token));
			return <Navigate to="/" />
		} catch (err) {
			dispatch(LoginFailure());

			if (typeof err.response.data.message === 'string') {
				toast.error(err.response.data.message);
			} else {
				AlertDispatch(AlertShow(err.response.data.message));
			}
		}
	};

	return (
		<Grid container component="main" sx={{ height: '100vh' }}>
			<ToastContainer />
			<CssBaseline />
			<Grid item xs={12} sm={4} md={7}
				sx={{
					backgroundImage: 'url(https://source.unsplash.com/random)',
					backgroundRepeat: 'no-repeat',
					backgroundColor: (t) =>
						t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				}}
			/>
			<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
				<Box
					sx={{
						my: 8,
						mx: 4,
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
					}}
				>
					<Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					<Box component="form" noValidate onSubmit={onSubmit} sx={{ mt: 1 }}>
						<TextField
							margin="normal"
							required
							fullWidth
							id="email"
							label="Email Address"
							name="email"
							autoComplete="email"
							autoFocus
							value={email}
							onChange={onChange}
						/>
						<TextField
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
							value={password}
							onChange={onChange}
						/>

						<Button
							type="submit"
							fullWidth
							variant="contained"
							sx={{ mt: 3, mb: 2 }}
							color="primary"
							disabled={isLoading}
						>
							Sign In
						</Button>
						<Grid container>
							<Grid item xs>
								<Link href="#" variant="body2">
									Forgot password?
								</Link>
							</Grid>
							<Grid item>
								<Link href="register" variant="body2">
									{"Don't have an account? Sign Up"}
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Grid>
		</Grid>
	)
}

export default Login