import React, { useEffect, useState } from 'react'
import { useQuery } from 'react-query';
import axios from 'axios';

import { CircularProgress, Box, Container, Typography } from '@mui/material';

import { API_PATH } from '../utils/APIConstant';

function Home() {

	const [details, setDetails] = useState();
	const { isLoading, data, isSuccess, isError, error } = useQuery('details', () => axios.get(`${API_PATH.PROFILE}`));
	// `${API_PATH.PROFILE}`

	useEffect(() => {
		if (isSuccess) {
			setDetails(data.data);
		}
	}, [isSuccess, data]);

	if (isLoading) {
		return (
			<Box sx={{ display: 'flex' }}>
				<CircularProgress />
			</Box>
		)
	}

	if (isError) {
		return <div>{error.msg}</div>
	}

	if (isSuccess && details) {
		return (
			<Container maxWidth="md">
				<h1 style={{ textAlign: 'center' }}>Welcome {details.profile.name}</h1>
				<Box sx={{ display: 'flex' }}>
					<Box sx={{ flexGrow: 1 }}>
						<Typography variant="h6" component="h1" gutterBottom>
							Name: {details.profile.name}
						</Typography>
						<Typography variant="h6" component="h2" gutterBottom>
							Gender: {details.profile.gender}
						</Typography>
						<Typography variant="h6" component="h2" gutterBottom>
							Age: {details.profile.age}
						</Typography>
						<Typography variant="h6" component="h2" gutterBottom>
							Date of Birth: {details.profile.dob}
						</Typography>
					</Box>
				</Box>
			</Container>
		)
	}
}

export default Home