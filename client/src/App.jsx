import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import NoPage from './pages/NoPage';
import AuthRoute from './utils/AuthRoute';
import ProtectRoute from './utils/ProtectRoute';
import AlertToast from './components/AlertToast';

import Navbar from './components/Navbar';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <AlertToast />
      <Routes>
        <Route path="/" element={<AuthRoute><Home /></AuthRoute>} />
        <Route path="/login" element={<ProtectRoute><Login /></ProtectRoute>} />
        <Route path="/register" element={<ProtectRoute><Register /></ProtectRoute>} />
        <Route path="*" element={<NoPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
