import React, { useContext } from 'react'
import { toast, ToastContainer } from 'react-toastify'
import { AlertContext } from '../storage/Context/AlertContext';

function AlertToast() {

    const { alerts } = useContext(AlertContext);

    alerts != null && alerts.map(alert => {
        console.log("in AlertToast", alert);
        toast.error(alert.msg);
        return(
            <ToastContainer />
        )
    })
}

export default AlertToast