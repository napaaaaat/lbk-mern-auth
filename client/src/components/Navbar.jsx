import React, { useContext } from 'react';
import { Link, Navigate } from 'react-router-dom';

import { AppBar, Box, Toolbar, Typography, Button, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

import { AuthContext } from '../storage/Context/AuthContext';
import { Logout } from '../storage/Action/AuthAction';

export default function ButtonAppBar() {

    const { user, dispatch } = useContext(AuthContext);

    const handleLogout = () => {
        dispatch(Logout());
        return <Navigate to="/login" />
    }

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Hello
                    </Typography>
                    {
                        user ? (
                            <Button color="inherit" onClick={handleLogout}>Logout</Button>
                        ) : (
                            <Button color="inherit" component={Link} to="/login">Login</Button>
                        )
                    }
                </Toolbar>
            </AppBar>
        </Box>
    );
}

