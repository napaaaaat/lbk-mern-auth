import { Request, Response } from "express";
import { validationResult } from "express-validator";
import UserServices from "../../services/user.services";

const userServices = new UserServices();

export const register = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ message: errors.array(), error: true });
    }

    try {
        const { email, username, password, name, gender, age, dob } = req.body;
        const result = await userServices.SignUpUser({ email, username, password, name, gender, age, dob });
        
        if(result?.data.error) return res.status(400).json(result.data)
        return res.status(201).json(result?.data);

    } catch (error) {
        return res.status(500).json({ message: "Server Error" });
    }
}

export const signin = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ message: errors.array(), error: true });
    }

    try {
        const { email, password } = req.body;
        const result = await userServices.SignInUser({ email, password });

        if(result?.data.error) return res.status(400).json(result.data)
        return res.status(200).json(result?.data);

    } catch (error) {
        return res.status(500).json({ message: "Server Error" });
    }
}