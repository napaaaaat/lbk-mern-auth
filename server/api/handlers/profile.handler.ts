import { Request, Response } from "express";
import ProfileServices from "../../services/profile.services";

const profileServices = new ProfileServices();

export const getDetails = async (req: any, res: Response) => {
    try {
        const { id } = req.user;
        const result  = await profileServices.getDetails({ id });

        if(result?.data.error) return res.status(400).json(result.data);
        return res.status(200).json(result?.data);

    } catch (error) {
        return res.status(500).json({ message: "Server Error" });
    }
}