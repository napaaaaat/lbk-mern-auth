import { Application, Router } from "express";
import { check } from "express-validator";
import { register, signin } from "../handlers/user.handler";

const router = Router();

router.post("/register", [
    check("email", "Please enter valid email").isEmail(),
    check("password", "Password must be at least 6 characters").isLength({ min: 6 }),
    check("username", "please enter username").not().isEmpty(),
    check("name", "Name is required").not().isEmpty(),
    check("age", "Age is required").not().isEmpty(),
    check("gender", "Gender is required").isIn(['Male', 'Female', 'Other']),
    check("dob", "Date of birth is required").not().isEmpty(),
], register);

router.post("/signin", [
    check("email", "Please enter valid email").isEmail(),
    check("password", "Password must be at least 6 characters").isLength({ min: 6 }),
], signin);


export default router;
