import { Application, Router } from "express";
import { getDetails } from "../handlers/profile.handler";
import { auth } from "../middleware/auth";

const router = Router();

router.get("/me", auth, getDetails);

export default router;