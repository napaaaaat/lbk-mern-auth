import { NextFunction, Request, Response } from "express";
import { JwtPayload } from "jsonwebtoken";
import { Verifytoken } from "../../utils";

export interface UserAuthInfoRequest extends Request {
    user: string | JwtPayload
  }

export const auth = async (req: any, res: Response, next: NextFunction) => {

    try {
        //  get baerer token
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(401).json({ message: 'Unauthorized', error: true });
        }

        // verify token
        const payload = Verifytoken(token);
        if (!payload) {
            return res.status(401).json({ message: 'Invalid token', error: true  });
        }

        // attach user to request
        req.user = payload;
        next();
        
    } catch (error) {
        res.status(403).json({ message: 'Fail Authorized', error: true  });
    }
}