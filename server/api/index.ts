import { Application, Router } from 'express';
import user from './routes/user.router';
import profile from './routes/profile.router';

const routes = Router();

routes.use('/api/user', user);
routes.use('/api/profile', profile);

export default routes;