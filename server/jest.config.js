/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/__tests__/*.test.ts'],
  transform: {
    // "\\.[jt]sx?$": "babel-jest",
    '^.+\\.ts?$': 'ts-jest',
    "^.+\\.tsx?$": "ts-jest",
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
};