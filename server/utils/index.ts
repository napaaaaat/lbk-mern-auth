import jsonwebtoken from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

interface payloadInterface {
    id: string;
    email: string;
    username: string;
    isActive: boolean;
}

export const FormateData = (data: any) => {
    if (data) {
        return { data }
    } else {
        return { message: "No data found" }
    }
}

export const Generatetoken = (payload: payloadInterface) => {
    return jsonwebtoken.sign(payload, process.env.JWT_SECRET || "secret", { expiresIn: '3h' });
}

export const Verifytoken = (token: string) => {
    return jsonwebtoken.verify(token, process.env.JWT_SECRET || "secret");
}