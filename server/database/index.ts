import  UserEntities  from './entities/user.entities';
import ProfileEntities from './entities/profile.entities';
import { ProfileInterface } from './models/Profile';
import { UserInterface } from './models/User';
import  databaseConnection from './config/connectDB';

export {
    databaseConnection,
    UserEntities,
    UserInterface,
    ProfileEntities,
    ProfileInterface,
}