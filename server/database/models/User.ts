import mongoose from "mongoose";

export interface UserInterface {
    username?: string;
    password?: string;
    email?: string;
    createdAt?: Date;
    isActive?: boolean;
    hashedPassword?: string;
}

const UserSchema = new mongoose.Schema<UserInterface>({
    username: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    isActive: {
        type: Boolean,
        default: true,
    }
});

const UserModel = mongoose.model<UserInterface>("User", UserSchema);

export default UserModel;