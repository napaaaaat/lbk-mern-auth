import mongoose from "mongoose";

export interface ProfileInterface {
    user?: mongoose.Schema.Types.ObjectId | string;
    id?: mongoose.Schema.Types.ObjectId | string;
    name?: string;
    gender?: string;
    age?: number;
    dob?: string;
}

const ProfileSchema = new mongoose.Schema<ProfileInterface>({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required:true
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    dob: {
        type: String,
        required: true
    }
});

const ProfileModel = mongoose.model<ProfileInterface>("Profile", ProfileSchema);

export default ProfileModel;