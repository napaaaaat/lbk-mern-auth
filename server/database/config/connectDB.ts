import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

// type DBinterface = {
//     db: string;
// }


export default () => {

    const connect = () => {
        mongoose.connect(`${process.env.DB_URL}`).then(() => {
            console.log('MongoDB connected...');
        }
        ).catch(err => {
            console.log(err);
            process.exit(1);
        }
        );
    };

    connect();

    mongoose.connection.on('disconnected', connect);
};