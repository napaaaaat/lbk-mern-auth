import ProfileModel, { ProfileInterface } from "../models/Profile";

class ProfileEntities {

    async createProfile({ user, name, gender, age, dob }: ProfileInterface) {
        try {
            const profile = new ProfileModel({
                user: user,
                name,
                gender,
                age,
                dob,
            });

            const createdProfile = await profile.save();
            return createdProfile;
        } catch (error) {
            console.error(error);
        }
    }

    async getProfileByUser({ id }: ProfileInterface) {
        try {
            const profile = await ProfileModel.findOne({ user: id }).populate('user',['email', 'username', 'isActive']);
            return profile;
        } catch (error) {
            console.error(error);
        }
    }
}

export default ProfileEntities;