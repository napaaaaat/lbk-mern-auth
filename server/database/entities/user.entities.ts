import UserModel, { UserInterface } from "../models/User";

class UserEntities {

    async createUser({ email, username, hashedPassword }: UserInterface) {
        try {
            const user = new UserModel({
                email,
                username,
                password: hashedPassword,
                isActive: true,
            });

            const createdUser = await user.save();
            return createdUser;
        } catch (error) {
            console.error(error);
        } 
    }

    async getUserByEmail({ email }: UserInterface) {
        try {
            const user = await UserModel.findOne({ email });
            return user;
        } catch (error) {
            console.error(error);
        } 
    }

    async getMe({ email }: UserInterface) {
        try {
            const user = await UserModel.findOne({ email }).select("-password");
            return user;
        } catch (error) {
            console.error(error);
        }
    }
}

export default UserEntities;




