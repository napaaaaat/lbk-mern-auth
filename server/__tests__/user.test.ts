import request from 'supertest';
import app from '../index';
import User from '../database/models/User';
import Profile from '../database/models/Profile';
import mongoose from 'mongoose';

describe('Test User Services', () => {

    describe('Register User', () => {
        const profile = {
            name: 'test',
            gender: 'Female',
            age: 0,
            dob: '2000-20-11'
        }

        it('Register User Successfully; should return status 201', async () => {
            const user = {
                email: 'test@mail.com',
                username: 'test',
                password: '123456'
            }

            const data = {...user, ...profile}

            const mockCreateUser = jest.fn(() => user)
            const mockCreateProfile = jest.fn(() => profile)

            jest.spyOn(User.prototype, 'save').mockImplementation(() => mockCreateUser());
            jest.spyOn(Profile.prototype, 'save').mockImplementation(() => mockCreateProfile());

            const response = await request(app)
                .post('/api/user/register')
                .send(data);
            expect(response.status).toBe(201);
            expect(response.body).toHaveProperty('token');
            expect(response.body.message).toBe('User created successfully');
        });

        it('Email already exist; should return status 400', async () => {
            const user = {
                email: 'barry@mail.com',
                username: 'test',
                password: '123456'
            }

            const data = {...user, ...profile}

            const mockCreateUser = jest.fn(() => user)
            const mockCreateProfile = jest.fn(() => profile)

            jest.spyOn(User.prototype, 'save').mockImplementation(() => mockCreateUser());
            jest.spyOn(Profile.prototype, 'save').mockImplementation(() => mockCreateProfile());

            const response = await request(app)
                .post('/api/user/register')
                .send(data);
            expect(response.status).toBe(400);
            expect(response.body).toHaveProperty('error');
            expect(response.body.message).toBe('Email already exist');
        });

        it('Something wrong with ValidationChain; should return status 422', async () => {
            const user = {
                email: 'test@',
                username: 'test',
                password: '123'
            }

            const data = {...user, ...profile}
            
            const mockCreateUser = jest.fn(() => user)
            const mockCreateProfile = jest.fn(() => profile)

            jest.spyOn(User.prototype, 'save').mockImplementation(() => mockCreateUser());
            jest.spyOn(Profile.prototype, 'save').mockImplementation(() => mockCreateProfile());

            const response = await request(app)
                .post('/api/user/register')
                .send(data);
            expect(response.status).toBe(422);
            expect(response.body).toHaveProperty('error');
        });
    });

    describe('Signin User', () => {
        it('Signin User Successfully; should return status 200', async () => {
            const user = {
                email: 'barry@mail.com',
                password: '123456'
            }

            const response = await request(app)
                .post('/api/user/signin')
                .send(user);

            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('token');
            expect(response.body.message).toBe('User signed in successfully');
        });

        it('Email not exist; should return status 400', async () => {
            const user = {
                email: 'test@mail.com',
                password: '123456'
            }

            const response = await request(app)
                .post('/api/user/signin')
                .send(user);
            expect(response.status).toBe(400);
            expect(response.body).toHaveProperty('error');
            expect(response.body.message).toBe('Wrong Credentials');
        });

        it('Password is incorrect; should return status 400', async () => {
            const user = {
                email: 'barry@mail.com',
                password: '1234567'
            }

            const response = await request(app)
                .post('/api/user/signin')
                .send(user);
            expect(response.status).toBe(400);
            expect(response.body).toHaveProperty('error');
            expect(response.body.message).toBe('Wrong Credentials');
        });
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });
    
});
