import express, { Application } from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import {databaseConnection} from './database'
import routes from './api';
import Logger from './lib/logger';
import { limiter } from './utils/rateLimiter';

dotenv.config();

databaseConnection();

const app: Application = express();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(limiter);

app.use(routes);

app.get('/', (req, res) => {
    res.send({ message: 'Hello World'});
});

app.get("/logger", (_, res) => {
    Logger.error("This is an error log");
    // Logger.warn("This is a warn log");
    Logger.info("This is a info log");
    // Logger.http("This is a http log");
    // Logger.debug("This is a debug log");
  
    res.send("Hello world");
});

app.listen(process.env.PORT, () => {
    console.log(`Server is running on http://localhost:${process.env.PORT}`);
    }
);

export default app;
