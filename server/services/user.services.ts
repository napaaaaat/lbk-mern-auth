import { UserEntities, ProfileEntities, UserInterface } from '../database';
import bcrypt from 'bcrypt';

import { FormateData, Generatetoken } from '../utils';
import Logger from '../lib/logger';

interface payloadInterface {
    id: string;
    email: string;
    username: string;
    isActive: boolean;
}

interface SignUpUserInterface extends UserInterface {
    name: string;
    gender: string;
    age: number;
    dob: Date;
}

class UserServices {

    userEntity: any;
    profileEntity: any

    constructor() {
        this.userEntity = new UserEntities();
        this.profileEntity = new ProfileEntities();
    }

    Packpayload(user: payloadInterface) {
        return {
            id: user.id,
            email: user.email,
            username: user.username,
            isActive: user.isActive
        }
    }

    async SignUpUser({ email, username, password, name, gender, age, dob }: SignUpUserInterface) {

        try {
            // check email already exist
            const existCustomer = await this.userEntity.getUserByEmail({ email });
            if (existCustomer) {
                return FormateData({ message: 'Email already exist', error: true });
            }

            // hash password
            const hashedPassword = await bcrypt.hash(password as string, 10);

            // create user and profile
            const createdUser = await this.userEntity.createUser({ email, username, hashedPassword });
            const createdProfile = await this.profileEntity.createProfile({ user: createdUser._id, name, gender, age, dob });
            
            if(createdUser && createdProfile) {
                // create payload and token
                const payload = this.Packpayload(createdUser);
                const token = Generatetoken(payload);
                return FormateData({ message: 'User created successfully', token });
            }
            return FormateData({ message: 'User created failed', error: true });

        } catch (error) {
            console.log(error);
        }
    }

    async SignInUser({ email, password }: UserInterface) {
        try {
            // check email already exist
            const existUser = await this.userEntity.getUserByEmail({ email });
            if (!existUser) {
                return FormateData({ message: 'Wrong Credentials', error: true });
            }

            // check password
            const isPasswordValid = await bcrypt.compare(password as string, existUser.password);
            if (!isPasswordValid) {
                return FormateData({ message: 'Wrong Credentials', error: true });
            }

            // create payload
            const payload = this.Packpayload(existUser);
            // create token
            const token = Generatetoken(payload);

            Logger.info(`User [${existUser.username}] logged in`);
            return FormateData({ message: 'User signed in successfully', token });
            
        } catch (error) {
            console.log(error);
        }
    }
}

export default UserServices;