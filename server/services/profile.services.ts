import { ProfileEntities, ProfileInterface } from "../database";
import { FormateData } from "../utils";
import mongoose from "mongoose";

class ProfileServices {
    
    profileEntity: any;

    constructor() {
        this.profileEntity = new ProfileEntities();
    }

    async getDetails({ id }: ProfileInterface) {
        if(!mongoose.Types.ObjectId.isValid(id as string)){
            return FormateData({ message: 'Invalid ID'});
        }

        try {
            const profile = await this.profileEntity.getProfileByUser({ id });

            if(!profile) return FormateData({ message: 'Profile not found', error: true });
            return FormateData({ message: 'Profile found', profile });

        } catch (error) {
            console.error(error);
        }
    }
}

export default ProfileServices;